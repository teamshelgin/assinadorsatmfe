package ESS;

//import static ESS.CESS.equipamento;
import model.Equipamento;
import model.Equipamentos;
import java.io.*;
import java.nio.file.Files;
import java.util.*;

import java.security.cert.CertificateException;
import java.security.KeyStoreException;
import java.security.cert.X509Certificate;

import java.security.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.crypto.Cipher;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

//Teste
public class CESS {

    static final boolean DEBUGGING = java.lang.management.ManagementFactory.getRuntimeMXBean().getInputArguments().toString().toLowerCase().indexOf("-xdebug") > 0;
    //static final boolean DEBUGGING = false;

    static Equipamentos equipamentos;
    static Equipamento equipamento;

    static String pass;
    static Provider p;
    static KeyStore ks;
    static PrivateKey pk;
    
    public static byte[] getMD5(String filename) {
        MessageDigest complete = null;
        try (InputStream fis = new FileInputStream(filename)) {
            byte[] buffer = new byte[1024];
            complete = MessageDigest.getInstance("MD5");
            int numRead;
            do {
                numRead = fis.read(buffer);
                if (numRead > 0) {
                    complete.update(buffer, 0, numRead);
                }
            } while (numRead != -1);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ESS.CESS.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ESS.CESS.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ESS.CESS.class.getName()).log(Level.SEVERE, null, ex);
        }
        return complete != null ? complete.digest() : null;
    }

    public static String getMD5Checksum(String filename) {
        byte[] b = getMD5(filename);
        String result = "";

        if (b != null) {
            for (int i = 0; i < b.length; i++) {
                result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
            }
        }

        return result;
    }    

    private static boolean initializecard() {

        try {
            //Create our certificates from our CAC Card
            p = new sun.security.pkcs11.SunPKCS11("card.config.txt");
            Security.addProvider(p);

            char[] pin = pass.toCharArray();

            //KeyStore ks = null;
            ks = KeyStore.getInstance("PKCS11", p);
            ks.load(null, pin);

            showInfoAboutCAC(ks);

            pk = getPrivateKey(ks, pass);

            return true;
        } catch (Exception ex) {
            System.out.println(
                    "\nERRO AO INSTANCIAR O CERTIFICADO DIGITAL:\n"
                    + "Verifique se ele está inserido e se a senha informada esta correta.\n"
                    + ex.getMessage()
            );

            return false;
        }
    }

    public static String doPKCS1(PrivateKey k, Provider p, String m) throws Exception {
        String result = null;
        Cipher rsaCipher = Cipher.getInstance("RSA", p);
        rsaCipher.init(Cipher.ENCRYPT_MODE, k);
        byte[] encrypteddata = rsaCipher.doFinal(m.getBytes());
        result = new sun.misc.BASE64Encoder().encode(encrypteddata);
        return result;
    }

    public static String doSign(PrivateKey k, Provider p, String m) {
        String result = null;
        try {
            Signature signature = Signature.getInstance("SHA256withRSA", p);
            signature.initSign(k);
            byte[] message = m.getBytes();
            signature.update(message);
            byte[] sigBytes = signature.sign();
            result = new sun.misc.BASE64Encoder().encode(sigBytes);
        } catch (Exception e) {
        }
        return result;
    }

    public static PrivateKey getPrivateKey(KeyStore ks, String pass) throws KeyStoreException {
        /* Considerando a existência de uma única cadeia de certificados */
        Enumeration<String> aliases = ks.aliases();
        String alias = aliases.nextElement();
        PrivateKey pk = null;
        try {
            pk = (PrivateKey) ks.getKey(alias, pass.toCharArray());
        } catch (Exception e) {
        }

        return pk;
    }

    public static void showInfoAboutCAC(KeyStore ks) throws KeyStoreException, CertificateException {
        Enumeration<String> aliases = ks.aliases();

        while (aliases.hasMoreElements()) {
            String alias = aliases.nextElement();
            X509Certificate[] cchain = (X509Certificate[]) ks.getCertificateChain(alias);

            System.out.println("Certificate Chain for : " + alias);
            for (int i = 0; i < cchain.length; i++) {
                System.out.println(i + " SubjectDN: " + cchain[i].getSubjectDN());
                System.out.println(i + " IssuerDN:  " + cchain[i].getIssuerDN());
            }
        }
    }

    /*
    private static boolean processfile() {
        InputStream fin;
        BufferedReader filebuf;
        String line;
        Boolean ret_initcard;

        try {
            if (!DEBUGGING) {
                ret_initcard = initializecard();
            } else {
                ret_initcard = Boolean.TRUE;
            }

            if (ret_initcard) {
                fin = new FileInputStream("numerosserieteste_D.txt");
                filebuf = new BufferedReader(new InputStreamReader(fin));

                PrintWriter fileout_unix = null;

                PrintWriter fileout = new PrintWriter(equipamento.getNomearquivo());

                if ("1".equals(equipamento.getCodigo())) {
                    String nomeaux = equipamento.getNomearquivosaida() + ".unix";
                    nomeaux += "." + equipamento.getExtensaoarquivosaida();

                    fileout_unix = new PrintWriter(nomeaux);
                }

                long count = 0;
                while ((line = filebuf.readLine()) != null) {
                    //if (count > 0) {
                    //    fileout.print("\r\n");
                    //}

                    line = line.replaceAll("\r", "").replaceAll("\n", "").replace(".", "");
                    String[] nseg = line.split(";");

                    String result = (!DEBUGGING) ? doSign(pk, p, nseg[1]) : nseg[1] + " assinado";

                    result = result.replaceAll("\r", "").replaceAll("\n", "");
                    result = equipamento.toString() + ";" + nseg[0] + ";" + result;

                    if (count < filebuf.lines().count()) {
                        result = result + "\r\n";
                    }

                    fileout.print(result);

                    if (fileout_unix != null) {
                        fileout_unix.write(result + "\r");
                    }

                    System.out.println("Linha " + (count + 1) + " processada: " + ((!DEBUGGING) ? result.substring(0, 29) + "..." : result));

                    count++;
                }

                fileout.print(".");
                if (fileout_unix != null) {
                    fileout_unix.print("FIMDEARQUIVO");
                    fileout_unix.close();
                }

                filebuf.close();
                fileout.close();

                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            System.out.println(
                    "\nERRO DE ASSINATURA:\n"
                    + ex.getMessage()
            );

            return false;
        }
    }
     */
    private static boolean processfile() {
        Boolean ret_initcard;

        try {
            if (!DEBUGGING) {
                ret_initcard = initializecard();
            } else {
                ret_initcard = Boolean.TRUE;
            }

            if (ret_initcard) {
                List<String> lines_file_unsigned = Files.readAllLines(new File("numerosserieteste_D.txt").toPath());

                List<String> lines_file_signed = new ArrayList<>();
                List<String> lines_file_signed_unix = new ArrayList<>();

                for (int i = 0; i < lines_file_unsigned.size(); i++) {
                    String line = lines_file_unsigned.get(i);

                    line = line.replaceAll("\r", "").replaceAll("\n", "").replace(".", "");
                    String[] nseg = line.split(";");

                    String sign = (!DEBUGGING) ? doSign(pk, p, nseg[1]) : nseg[1] + " assinado";

                    sign = sign.replaceAll("\r", "").replaceAll("\n", "");
                    sign = equipamento.toString() + ";" + nseg[0] + ";" + sign;

                    lines_file_signed.add(sign);
                    lines_file_signed_unix.add(sign);

                    System.out.println("Linha " + (i + 1) + " assinada: " + ((!DEBUGGING) ? sign.substring(0, 29) + "..." : sign));
                }

                if (lines_file_unsigned.size() > 0) {
                    int last_line_idx = lines_file_unsigned.size() - 1;

                    lines_file_signed.set(last_line_idx, lines_file_signed.get(last_line_idx) + ".");
                    lines_file_signed_unix.add("FIMDEARQUIVO");
                    lines_file_signed_unix.add("");
                }

                Files.write(
                        new File(equipamento.getNomearquivo()).toPath(),
                        lines_file_signed.stream().map(Object::toString).collect(
                                Collectors.joining("\r\n")
                        ).getBytes()
                );
                
                if (equipamento.getGeraarquivounix().equals("1")) {
                    Files.write(
                            new File(equipamento.getNomearquivounix()).toPath(),
                            lines_file_signed_unix.stream().map(Object::toString).collect(
                                    Collectors.joining("\n")
                            ).getBytes()
                    );
                }
                
                System.out.println(
                    "\nMD5: " + getMD5Checksum(equipamento.getNomearquivo()).toUpperCase() + "\n"
                );
                
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            System.out.println(
                    "\nERRO DE ASSINATURA:\n"
                    + ex.getMessage()
            );

            return false;
        }
    }

    public static void main(String arg[]) throws Exception {
        String resposta;

        JAXBContext context;
        Unmarshaller unmarshaller;

        //* PRG - 1.4 - Encoding para caracteres especiais
        //Scanner s = new Scanner(System.in);        
        Scanner s = new Scanner(System.in, "Windows-1252");        
        //*

        pass = "";

        if (!DEBUGGING) {
            if (!new File("card.config.txt").isFile()) {
                System.out.println("O arquivo 'card.config.txt' não foi encontrado.");
                return;
            }
        }

        if (!new File("numerosserieteste_D.txt").isFile()) {
            System.out.println("O arquivo 'numerosserieteste_D.txt' não foi encontrado.");
            return;
        }

        if (!new File("equipamentos.xml").isFile()) {
            System.out.println("O arquivo 'equipamentos.xml' não foi encontrado.");
            return;
        }

        context = JAXBContext.newInstance(Equipamentos.class);
        unmarshaller = context.createUnmarshaller();

        try {
            equipamentos = (Equipamentos) unmarshaller.unmarshal(new FileInputStream("equipamentos.xml"));
        } catch (Exception ex) {
            System.out.println("O arquivo 'equipamentos.xml' é inválido.");
            return;
        }

        equipamentos.getEquipamentos().sort(
                (Equipamento e1, Equipamento e2)
                //-> e1.getCodigo().compareToIgnoreCase(e2.getCodigo())
                -> e1.getCodigoAsInteger()- e2.getCodigoAsInteger()
        );

        String question = "";
        for (int i = 0; i < equipamentos.getEquipamentos().size(); i++) {
            question += String.valueOf(i + 1) + ") " + equipamentos.getEquipamentos().get(i).getNome();

            if (i < equipamentos.getEquipamentos().size() - 1) {
                question += "\n";
            }
        }

        question = "Selecione um equipamento: \n\n" + question + "\nC) para cancelar";

        do {
            System.out.println(question);
            resposta = s.next();

            List<String> options
                    = equipamentos.getEquipamentos().stream().map(Equipamento::getCodigo).collect(Collectors.toList());

            options.add("C");

            //if (Stream.of("1", "2", "3", "C").anyMatch(resposta::equalsIgnoreCase)) {
            if (options.stream().anyMatch(resposta::equalsIgnoreCase)) {
                break;
            } else {
                System.out.println("Opção inválida.");
            }
        } while (true);

        if ("c".equalsIgnoreCase(resposta)) {
            return;
        }

        equipamento = equipamentos.getEquipamentos().get(Integer.valueOf(resposta) - 1);

        System.out.println("\n" + equipamento.toStringHuman());

        question = "\nAssinar o arquivos com estas informações? (S para SIM - N para NÃO)";

        do {
            System.out.println(question);
            resposta = s.next();

            if (Stream.of("s", "n").anyMatch(resposta::equalsIgnoreCase)) {
                break;
            } else {
                System.out.println("Opção inválida.");
            }
        } while (true);

        if ("n".equalsIgnoreCase(resposta)) {
            return;
        }

        List<String> files_to_check = new ArrayList<>();

        files_to_check.add(equipamento.getNomearquivo());
        if (equipamento.getGeraarquivounix().equals("1")) {
            files_to_check.add(equipamento.getNomearquivounix());
        }

        for (int i = 0; i < files_to_check.size(); i++) {
            File file = new File(files_to_check.get(i));

            while (file.exists()) {
                System.out.println("O arquivo '" + file.getName() + "' já existe. Tentar novamente? (S para SIM - N para NÃO)");
                resposta = s.next();

                if (Stream.of("s", "n").anyMatch(resposta::equalsIgnoreCase)) {
                    if ("n".equalsIgnoreCase(resposta)) {
                        break;
                    }
                } else {
                    System.out.println("Opção inválida.");
                }
            }
/*
            if (file.exists()) {
                do {
                    System.out.println("O arquivo '" + equipamento.getNomearquivo() + "' já existe. Tentar novamente? (S para SIM - N para NÃO)");
                    resposta = s.next();

                    if (Stream.of("s", "n").anyMatch(resposta::equalsIgnoreCase)) {
                        if ("n".equalsIgnoreCase(resposta)) {
                            break;
                        }
                    } else {
                        System.out.println("Opção inválida.");
                    }
                } while (true);
            } else {
                files_to_check.remove(i);
            }
*/
        }

        if ("n".equalsIgnoreCase(resposta)) {
            return;
        }
                
        if (!DEBUGGING) {
            do {
                System.out.println("Digite a senha do certificado digital (C para cancelar):");
                pass = s.next();

                if (pass.trim().length() < 3 && !"c".equalsIgnoreCase(pass)) {
                    System.out.println("Senha deve conter pelo menos 3 caracteres.");
                } else {
                    break;
                }
            } while (true);
        }

        if ("c".equalsIgnoreCase(pass)) {
            return;
        }

        processfile();
    }
}
