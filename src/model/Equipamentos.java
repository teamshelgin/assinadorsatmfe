package model;

import java.util.List;
import java.util.stream.Collectors;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "equipamentos")
public class Equipamentos {

    @XmlElement(name = "equipamento")
    private List<Equipamento> listaequipamentos;

    public List<Equipamento> getEquipamentos() {
        return listaequipamentos;
    }

    void afterUnmarshal(Unmarshaller unmarshaller, Object parent) {
        listaequipamentos = listaequipamentos.stream()
                .filter(item -> Boolean.TRUE.equals(item.getValido())).collect(Collectors.toList());
    }
}
