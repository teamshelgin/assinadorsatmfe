package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SAT {

    private static final List<String> CAMPOSOBRIGATORIOS = Arrays.asList("Modelo", "VersaoSoftwareBasico");

    private final Equipamento equipamento;

    private String modelo;
    private String versaosoftwarebasico;
    private Boolean valido;

    public SAT(Equipamento equipamento) {
        this.equipamento = equipamento;
        
        List<String> parametros = Arrays.asList(equipamento.getParametros().split(";"));

        valido = Boolean.FALSE;

        if (parametros.size() == CAMPOSOBRIGATORIOS.size()) {
            List<String> campos = new ArrayList<>();

            for (String campo : parametros) {
                campos.addAll(Arrays.asList(campo.split("=")));
            }

            if ((campos.size() == (CAMPOSOBRIGATORIOS.size() * 2))
                    && (campos.containsAll(CAMPOSOBRIGATORIOS))) {
                modelo = campos.get(1);
                versaosoftwarebasico = campos.get(3);

                valido = Boolean.TRUE;
            }
        }
    }

    public String toStringHuman() {
        return "Modelo: " + modelo + ", SW Básico: " + versaosoftwarebasico;
    }

    @Override
    public String toString() {
        return modelo + ";" + versaosoftwarebasico;
    }

    //public String getNomearquivo() {
    //    return (new SimpleDateFormat("yyyyMMdd").format(new Date())) + " - " + equipamento.getNomearquivosaida() + ".TXT";
    //}

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getVersaosoftwarebasico() {
        return versaosoftwarebasico;
    }

    public void setVersaosoftwarebasico(String versaosoftwarebasico) {
        this.versaosoftwarebasico = versaosoftwarebasico;
    }

    public Boolean getValido() {
        return valido;
    }
}
