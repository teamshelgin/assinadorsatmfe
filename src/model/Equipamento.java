package model;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "equipamento")
public class Equipamento {

    private String codigo, nome, parametros, categoria, nomearquivosaida, extensaoarquivosaida, geraarquivounix;
    private Object dados;

    private Boolean valido;

    public Equipamento() {
        valido = Boolean.FALSE;
    }

    @Override
    public String toString() {
        return (dados instanceof SAT) ? ((SAT) dados).toString() : ((MFE) dados).toString();
    }

    public String toStringHuman() {
        return (dados instanceof SAT) ? ((SAT) dados).toStringHuman() : ((MFE) dados).toStringHuman();
    }

    void afterUnmarshal(Unmarshaller unmarshaller, Object parent) {
        if (!"".equals(parametros) && (!"".equals(categoria))) {
            switch (categoria) {
                case "SAT":
                    dados = new SAT(this);
                    valido = ((SAT) dados).getValido();
                    break;
                case "MFE":
                    dados = new MFE(this);
                    valido = ((MFE) dados).getValido();
                    break;
            }
        }
    }

    public String getNomearquivo() {
        //return (dados instanceof SAT) ? ((SAT) dados).getNomearquivo() : ((MFE) dados).getNomearquivo();
        return (new SimpleDateFormat("yyyyMMdd").format(new Date())) + " - " + getNomearquivosaida() + "." + getExtensaoarquivosaida();
    }

    public String getNomearquivounix() {
        return getNomearquivo() + ".unix." + getExtensaoarquivosaida();
        
        //return (new SimpleDateFormat("yyyyMMdd").format(new Date())) + " - " + getNomearquivosaida() + "." + getExtensaoarquivosaida();        
    }

    @XmlAttribute(name = "codigo")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @XmlAttribute(name = "nome")
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @XmlAttribute(name = "categoria")
    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @XmlAttribute(name = "nomearquivosaida")
    public String getNomearquivosaida() {
        return nomearquivosaida;
    }

    public void setNomearquivosaida(String nomearquivosaida) {
        this.nomearquivosaida = nomearquivosaida;
    }

    @XmlAttribute(name = "extensaoarquivosaida")
    public String getExtensaoarquivosaida() {
        return extensaoarquivosaida;
    }

    public void setExtensaoarquivosaida(String extensaoarquivosaida) {
        this.extensaoarquivosaida = extensaoarquivosaida;
    }

    @XmlAttribute(name = "geraarquivounix")
    public String getGeraarquivounix() {
        return geraarquivounix;
    }

    public void setGeraarquivounix(String geraarquivounix) {
        this.geraarquivounix = geraarquivounix;
    }

    @XmlElement(name = "parametros")
    public String getParametros() {
        return parametros;
    }

    public void setParametros(String parametros) {
        this.parametros = parametros;
    }

    public Boolean getValido() {
        return valido;
    }

    public Object getDados() {
        return dados;
    }
    
    public Integer getCodigoAsInteger() {
        return Integer.parseInt(getCodigo());
    }
}
